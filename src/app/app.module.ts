import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule, Routes} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule, Http} from '@angular/http';

import {AppComponent} from './app.component';
import {Page1Component} from './page1/page1.component';
import {Page2Component} from './page2/page2.component';
import {ConfirmDialogComponent} from './confirmation/confirm-dialog.component';

import {ConfirmDialogService} from './services/confirm-dialog.service';
import {CanDeactivateGuard} from './services/can-deactivate-guard.service';

const appRoutes: Routes = [
  {path: 'page1', component: Page1Component, canDeactivate: [CanDeactivateGuard]},
  {path: 'page2', component: Page2Component, canDeactivate: [CanDeactivateGuard]}
];

@NgModule({
  declarations: [
    AppComponent,
    Page1Component,
    Page2Component,
    ConfirmDialogComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ConfirmDialogService, CanDeactivateGuard],
  bootstrap: [AppComponent],
})
export class AppModule {
}
