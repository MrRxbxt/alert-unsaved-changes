import { Component } from '@angular/core';
import { ConfirmDialogService } from "../services/confirm-dialog.service";
import { Observable } from 'rxjs';

@Component({
  selector: 'page1',
  templateUrl: './page1.component.html',
  styleUrls: []
})
export class Page1Component {

  firstName: string;
  lastName: string;

  constructor(private confirmDialogService: ConfirmDialogService) {}

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!(this.firstName || this.lastName)) {
      return true;
    }
    this.confirmDialogService.open();
    return this.confirmDialogService.modalData;
  }
}
