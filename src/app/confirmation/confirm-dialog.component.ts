import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ConfirmDialogService } from '../services/confirm-dialog.service';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirm-dialog.component.html',
  styles: ['./confirm-dialog.component.scss'],
})

export class ConfirmDialogComponent implements OnInit {
  private isModalOpen: boolean;
  private title: string;
  private messageBody: string;

  constructor(private confirmDialogService: ConfirmDialogService) { }

  ngOnInit() {
    this.confirmDialogService.getModalStatus().subscribe(data => this.isModalOpen = data as boolean);
    this.title = this.confirmDialogService.getModalTitle();
    this.messageBody = this.confirmDialogService.getMessageBody();
  }

  setConfirmStatus(confirmStatus: boolean): void {
    this.confirmDialogService.setConfirmStatus(confirmStatus);
  }

  closeModal() {
    this.confirmDialogService.close();
  }
}
