import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ConfirmDialogService {

    modalData: Subject<boolean> = new Subject<boolean>();

    constructor() {

    }

    open() {
        this.modalData.next(true);
    }

    close() {
        this.modalData.next(false);
    }

    setConfirmStatus(confirmStatus: boolean) {
        this.modalData.next(confirmStatus);
    }

    getModalStatus(): Observable<boolean> {
        return this.modalData.asObservable();
    }

    getModalTitle(): string {
        return "Unsaved Changes";
    }

    getMessageBody(): string {
        return "There are unsaved changes.";
    }
}
