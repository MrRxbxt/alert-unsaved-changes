
import { Component } from '@angular/core';
import { ConfirmDialogService } from '../services/confirm-dialog.service';
import { Observable } from 'rxjs';

import { from } from 'rxjs/observable/from';
@Component({
  selector: 'page2',
  templateUrl: './page2.component.html',
  styleUrls: []
})

export class Page2Component {
  
  gender: string;
  address: string;
  contactNo: string;

  constructor(private confirmDialogService: ConfirmDialogService) {
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!(this.gender || this.address || this.contactNo)) {
      return true;
    }
    this.confirmDialogService.open();
    return this.confirmDialogService.modalData;
  }
}
